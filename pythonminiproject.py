from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
from tkinter import ScrolledText
from tkinter.filedialog import asksaveasfilename
from tkinter import font
from tkinter import colorchooser
import os.path


root=Tk()
root.title("Text Editor")
root.geometry("1200x700")
f1=Frame(root,bg='#8D6E63',borderwidth=3,relief="raised")
f1.pack(side='top',fill=X)
img=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon1.png")
img1=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon2.png")
img2=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon3.png")
img3=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon4.png")
img4=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon5.png")
img5=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon6.png")
img6=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon7.png")
img7=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon8.png")
img8=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon9.png")
img9=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon10.png")
img10=PhotoImage(file="C:/Users/sarat/Desktop/PythonProject/icon11.png")

   

def select():
    print("Hi")
    textpad.tag_add(SEL, "1.0", END)

def bold():
    try:
        text=textpad.get(SEL_FIRST,SEL_LAST)
        textpad.delete(SEL_FIRST,SEL_LAST)
    except TclError:
        messagebox.showwarning("Warning","Select text")
        return
    textpad.configure(font=("Times",15,"bold")) #not what i had in mind.... try running it
    textpad.insert(INSERT,text)

def copy():
    try:
        text=textpad.get(SEL_FIRST,SEL_LAST)
    except TclError:
        messagebox.showwarning("Warning","Select text")
        return
    root.clipboard_clear()
    root.clipboard_append(text)

def paste():
    text=root.clipboard_get()
    textpad.insert(INSERT,text)

def framechanger():
    col=colorchooser.askcolor()
    f1.configure(bg=col[1]) #askcolor() returns tuple as ((R,B,G),'hexadeximal')



def new():
    textpad.delete('1.0',END)

def exit():
    r=messagebox.askyesno("Confirm","Are you sure you want to exit? All progress will be lost")
    print(r)
    if r!=False:
        root.destroy()
        
def openfile():
    fileopen=filedialog.askopenfile(parent=root,mode='rb',title='Select a file')
    if fileopen!= None:
            textpad.delete('1.0',END)
            text=fileopen.read()
            textpad.insert('1.0',text)
            fileopen.close()

def save():
    file_name=asksaveasfilename()
    if os.path.exists(file_name):
        f=open(file_name,'a');
    else:
        f=open(file_name+'.txt','w');
    text=textpad.get('1.0',END)
    f.write(text)
    f.close()

def about():
    f=open("C:/Users/sarat/Desktop/PythonProject/About.txt",'r')
    text=f.read()
    textpad.insert('1.0',text)
    f.close()

def font():
    try:
        text=textpad.get(SEL_FIRST,SEL_LAST)
        textpad.delete(SEL_FIRST,SEL_LAST)
    except TclError:
        messagebox.showwarning("Warning","Select text")
        return
    textpad.configure(font=(variable.get(),int(variable1.get()))) 
    textpad.insert(INSERT,text)
    




OPTIONS=["Arial","Times New Roman","Helvetica"]
variable=StringVar(f1)
variable.set(OPTIONS[0])
w=OptionMenu(f1,variable,*OPTIONS)
w.pack(side="left")

OPTIONS=["5","10","12","14","18","24","36","72"]
variable1=StringVar(f1)
variable1.set(OPTIONS[0])
w=OptionMenu(f1,variable1,*OPTIONS)
w.pack(side="left")
listb1=Button(f1,text="OK",command=font,image=img10)
listb1.pack(side="left")

    

b8=Button(f1,text="New",image=img7,command=new,borderwidth=3,relief="raised")
b8.pack(side="left")
b9=Button(f1,text="Open",image=img8,command=openfile,borderwidth=3,relief="raised")
b9.pack(side="left")
b10=Button(f1,text="Save As",image=img9,command=save,borderwidth=3,relief="raised")
b10.pack(side="left")
b1=Button(f1,text="Select",image=img,bg="black",command=select,borderwidth=3,relief="raised")
b1.pack(side="left")
b2=Button(f1,text="Bold",image=img1,command=bold,borderwidth=3,relief="raised")
b2.pack(side="left")
b3=Button(f1,text="Color Change",image=img2,command=framechanger,borderwidth=3,relief="raised")
b3.pack(side="left")
b4=Button(f1,text="Copy",image=img3,command=copy,borderwidth=3,relief="raised")
b4.pack(side="left")
b5=Button(f1,text="Paste",image=img4,command=paste,borderwidth=3,relief="raised")
b5.pack(side="left")
b6=Button(f1,text="About",image=img5,command=about,borderwidth=3,relief="raised")
b6.pack(side="left")
b7=Button(f1,text="Exit",image=img6,command=exit,borderwidth=3,relief="raised")
b7.pack(side="left")





textpad=ScrolledText(root,width=150,height=130,font=("Times",15))   #run it!!!
textpad.pack()






    

m1=Menu(root)
filem=Menu(m1,tearoff=0)

filem.add_command(label="New",command=new,activebackground="#a35811")
filem.add_command(label="Open",command=openfile,activebackground="#a35811")
filem.add_command(label="Save As",command=save,accelerator="Ctrl+O",activebackground="#a35811")
filem.add_separator()
filem.add_command(label="Exit",command=exit,activebackground="#a35811")
m1.add_cascade(label="File",menu=filem,underline=0)


editm=Menu(m1,tearoff=0)
editm.add_command(label="Copy",command=copy,activebackground="#a35811")
editm.add_command(label="Paste",command=paste,activebackground="#a35811")
m1.add_cascade(label="Edit",menu=editm,underline=0)

aboutm=Menu(m1,tearoff=0)
aboutm.add_command(label="Info",command=about,activebackground="#a35811")
m1.add_cascade(label="About",menu=aboutm,underline=0)
root.config(menu = m1)

root.mainloop()
